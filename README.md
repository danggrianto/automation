# Setting up automation project from scratch
## tools needed
* maven
* selenium
* testNG
* grid
* jenkins
* phanthom.js
* virtualbox
* ghostdriver

## repository
The first thing to do in this project is to setup a repository. I use [bitbucket](https://bitbucket.org/) to setup my repository.
1. create repository and put information about your repository
2. choose Git as repository type 
3. choose language (in his project i use java)
![create repository](http://screencast.com/t/qlc50kJ2TYan)
4. clone project to your machine
	mkdir /path/to/your/project
	cd /path/to/your/project
	git init
	git remote add origin ssh://git@bitbucket.org/<your-repository>/automation.git
![create local repository](http://screencast.com/t/8INl4TBQ2)

## adding selenium to the project
add selenium dependency on pom.xml
    <dependency>
      <groupId>org.seleniumhq.selenium</groupId>
      <artifactId>selenium-java</artifactId>
      <version>2.33.0</version>
      <scope>test</scope>
    </dependency>

## adding testNG to the project
add testNG dependency to the project
    <dependency>
      <groupId>org.testng</groupId>
      <artifactId>testng</artifactId>
      <version>6.1.1</version>
      <scope>test</scope>
    </dependency>

## adding phantom.js / ghost driver to selenium project
add this dependency to pom.xml
    <dependency>
        <groupId>com.github.detro.ghostdriver</groupId>
        <artifactId>phantomjsdriver</artifactId>
        <version>1.0.3</version>
    </dependency>

install phantom.js to your computer
on mac install [homebrew](http://mxcl.github.io/homebrew/)
    ruby -e "$(curl -fsSL https://raw.github.com/mxcl/homebrew/go)"
install phantom.js using homebrew
    brew install phantomjs
run phantom.js
    phantomjs --webdriver=4444
run webdriver test using PhantomJSDriver
    example code
    public class WebDriverTest {
        private WebDriver driver;

        @BeforeMethod
        public void setUp() throws MalformedURLException {
            DesiredCapabilities dc = DesiredCapabilities.phantomjs();
            dc.setJavascriptEnabled(true);
            driver = new RemoteWebDriver(new URL("http://localhost:4444"),dc);
        }
        @AfterMethod
        public void tearDown(){
            driver.quit();

        }

        @Test
        public void test(){
            driver.get("http://google.com");
        }

    }





