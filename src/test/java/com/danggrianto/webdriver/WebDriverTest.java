package com.danggrianto.webdriver;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.net.MalformedURLException;
import java.net.URL;

/**
 * User: danggrianto
 * Date: 7/13/13
 * Time: 10:58 AM
 */
public class WebDriverTest {
    private WebDriver driver;

    @BeforeMethod
    public void setUp() throws MalformedURLException {
        DesiredCapabilities dc = DesiredCapabilities.phantomjs();
        dc.setJavascriptEnabled(true);
        driver = new RemoteWebDriver(new URL("http://localhost:4444"),dc);
    }
    @AfterMethod
    public void tearDown(){
        driver.quit();

    }

    @Test
    public void test(){
        driver.get("http://google.com");
    }

}
